/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.Flush;
import es.tems.openholdem.hand.FourOfAKind;
import es.tems.openholdem.hand.FullHouse;
import es.tems.openholdem.hand.HighestCard;
import es.tems.openholdem.hand.OnePair;
import es.tems.openholdem.hand.Hand;
import es.tems.openholdem.hand.Straight;
import es.tems.openholdem.hand.StraightFlush;
import es.tems.openholdem.hand.ThreeOfAKind;
import es.tems.openholdem.hand.TwoPair;

import java.util.Iterator;

public class ShowdownAnalyzer {

	
	Set<Card> tablecards;
	List<Player> showdownPlayers;
	
	// Each element in orderedHands is a list of tied hands. BetManager is responsible for distributing pots 
	// according to this list and betting relationships
	List<List<Hand>>  orderedHands = new ArrayList<List<Hand>>();
	
	public ShowdownAnalyzer(List<Card> tableCards, List<Player> showdownPlayers) {
		this.tablecards=new HashSet<Card>();
		this.tablecards.addAll(tableCards);
		this.showdownPlayers= new ArrayList<Player>(showdownPlayers);
	}
	
	public List<List<Hand>> getOrderedHands(){

		checkForStraightFlush();
		checkFor4ofAKind();
		checkForFullHouse();
		checkForFlush();
		checkForStraight();
		checkForThreeOfAKind();
		checkForTwoPair();
		checkForOnePair();
		//TODO BUG: if all players go to highestCard...Trouble
		highestCard();
		return orderedHands;
	}

	private void highestCard() {
		checkForHand(HighestCard.class);
	}

	private void checkForOnePair() {
		checkForHand(OnePair.class);
	}

	private void checkForTwoPair() {
		checkForHand(TwoPair.class);
	}

	private void checkForThreeOfAKind() {
		checkForHand(ThreeOfAKind.class);
	}

	private void checkForStraight() {
		checkForHand(Straight.class);
	}
	private void checkForFlush() {
		checkForHand(Flush.class);
	}
	private void checkForFullHouse() {
		checkForHand(FullHouse.class);
		
	}
	private void checkFor4ofAKind() {
		checkForHand(FourOfAKind.class);
	}

	private void checkForStraightFlush() {
		checkForHand(StraightFlush.class);
	}
	
	private static final Class<?>[] parameterTypes = {Player.class, Set.class};
	
	private void checkForHand(Class<?> handclass) {
		List<Hand> hands = new ArrayList<Hand>();
		Iterator<Player> it=showdownPlayers.iterator();
		Player player;
		Hand hand;
		while(it.hasNext()){
			player=it.next();
			
			Constructor<?> ctor = null;
			try {
				ctor = handclass.getConstructor(parameterTypes);
				hand = (Hand) ctor.newInstance(player, tablecards);
			} catch (Exception e) {
				
				e.printStackTrace();
				return;
			}
			if (hand.hasHand()) {
				hands.add(hand);
				it.remove();
			}
		}
		
		if (hands.size()>0)
			orderedHands.addAll(Hand.sort(hands));
	}

	/**
	 * Returns a list of lists of players, ordered according to hand strength. Tied hands are grouped on the same player lists.
	 * 
	 * @return a list of lists of players, ordered according to hand strength
	 */
	public List<List<Player>> getWinners() {
		if (orderedHands.size()==0)	getOrderedHands();
		List<List<Player>> winners=new ArrayList<List<Player>>();
		for(List<Hand> tiedhands: orderedHands){
			List<Player> tiedPlayers = new ArrayList<Player>();
			for(Hand hand: tiedhands){
				tiedPlayers.add(hand.getPlayer());
			}
			winners.add(tiedPlayers);
		}
		return winners;
	}


	


}
