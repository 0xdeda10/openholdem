/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.event;

import es.tems.openholdem.Game;
import es.tems.openholdem.PlayerAction;

/**
 * 
 * Represents a Player action event
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
	public class PlayerActionEvent extends GameEvent {
	public static final short PLAYER_ACTION =2;
	private PlayerAction playerAction;
	
	public PlayerActionEvent(Game game,PlayerAction playerAction) {
		super(game);
		this.setType(PLAYER_ACTION);
		this.setPlayerAction(playerAction);
	}
	
	public PlayerAction getPlayerAction() {
		return playerAction;
	}
	public void setPlayerAction(PlayerAction playerAction) {
		this.playerAction = playerAction;
	}
}
