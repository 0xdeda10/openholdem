/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.event;

import es.tems.openholdem.Game;
import es.tems.openholdem.Player;

/**
 * 
 * Represents a Pot distribution event, one for each player that receives chips.
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class PotEvent extends GameEvent {

	public static final short POT = 4;
	
	Player player;
	int earnings;
	String potID;
	boolean tied;
	
	public PotEvent(Game game, Player player, int earnings, String potID,boolean tied) {
		super(game);
		this.setType(POT);
		this.setEarnings(earnings);
		this.setPlayer(player);
		this.setPotID(potID);
		this.tied=tied;
	}

	
	//Getters and Setters
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getEarnings() {
		return earnings;
	}
	public void setEarnings(int earnings) {
		this.earnings = earnings;
	}
	public String getPotID() {
		return potID;
	}

	public void setPotID(String potID) {
		this.potID = potID;
	}


	public boolean isTied() {
		return tied;
	}


	public void setTied(boolean tied) {
		this.tied = tied;
	}

}
