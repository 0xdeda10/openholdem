/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.event;

import java.util.Iterator;

import es.tems.openholdem.Player;
import es.tems.util.CircularArrayList;
/**
 *  This singleton class is in charge of communicating Game Events to Players .
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class NotificationManager {

	private static NotificationManager instance;
	
	private NotificationManager(){super();};
	
	public static NotificationManager getInstance() {
		if(instance==null) instance=new NotificationManager();
		return instance;
	}

//	public void notifyAll(PlayerAction pa) {
//		CircularArrayList<Player> players = this.getPlayers();
//		Iterator<Player> it = players.getArrayListIterator();
//		Player p;
//		while(it.hasNext()){
//			p = it.next();
//			p.notify(pa);
//		}
//	}
//	public void notifyAll(PlayerAction pa, int stake){
//		// Notify all players of action and new stake
//		CircularArrayList<Player> players = this.getPlayers();
//		Iterator<Player> it = players.getArrayListIterator();
//		Player p;
//		while(it.hasNext()){
//			p = it.next();
//			p.notify(pa,stake);
//		}
//	}

	public void notifyAll(GameEvent gameEvent) {
		CircularArrayList<Player> players = gameEvent.getGame().getPlayers();
		Iterator<Player> it = players.getArrayListIterator();
		Player p;
		while(it.hasNext()){
			p = it.next();
			p.notify(gameEvent);
		}
	}

}
