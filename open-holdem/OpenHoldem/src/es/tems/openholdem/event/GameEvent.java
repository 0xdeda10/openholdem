/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.event;

import es.tems.openholdem.Game;
import es.tems.openholdem.PlayerAction;

/**
 *  This class represents a Game Event enabling communication with Players and their user interfaces.
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class GameEvent {

	// Static constants and variables

	private Game game;
	private short type;
	private PlayerAction playerAction;
	

	public GameEvent(Game game) {
		this.game=game;
	}
	//Getters and Setters
	public short getType() {
		return type;
	}
	public void setType(short type) {
		this.type = type;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}

	

	

}
