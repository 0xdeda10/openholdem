/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.tems.openholdem.event.FlopEvent;
import es.tems.openholdem.event.NewPlayerEvent;
import es.tems.openholdem.event.NotificationManager;
import es.tems.openholdem.event.ShowEvent;
import es.tems.openholdem.exception.InconsistentStateException;
import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.FrenchDeck;
import es.tems.util.CircularArrayList;
/**
 *  This class represents the game. It follows the different phases and manages the card deck, players and spectators
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class Game {
	public static final short MAX_NUMBER_OF_PLAYERS = 8;
	private static final short MIN_NUM_PLAYERS = 2;

	public static final int DEFAULT_CHIPS = 500;

	private static final int MINIMUM_CHIPS = 0;

	private static final long CHECK4PLAYERS_INTERVAL = 8000;

	private static final boolean PAUSE_AFTER_HAND = true;

	private static final long AFTER_HAND_PAUSE = 10000;

	
	
	protected CircularArrayList<Player> players = new CircularArrayList<Player>();
	protected short button_player=0;
	protected FrenchDeck deck;
	protected List<Player> spectators = new ArrayList<Player>(); 
	//spectators will join automatically if there is a free place
	protected BetManager bm;
	protected List<Card> tableCards = new ArrayList<Card>();
	private List<List<Player>> winners;
	private Log log = Log.getInstance();
	
	public Game() {
		super();
		deck=new FrenchDeck();
		bm=new BetManager(this);
		
	}

	public void playHand() throws InconsistentStateException {
		log.info("Starting a new hand");
		while(players.size()<MIN_NUM_PLAYERS) {
			addPlayers();
			try {
				Thread.sleep(CHECK4PLAYERS_INTERVAL);
			} catch (InterruptedException e) {
				log.warn("Interrupted exception while waiting for players" + e.getMessage());
			}
		}
		dealAndBet();
		showdown();
		distributePots();
		if(PAUSE_AFTER_HAND)
			try {
				Thread.sleep(AFTER_HAND_PAUSE);
			} catch (InterruptedException e) {
				log.warn("Interrupted exception while in after hand pause" + e.getMessage());
			}
		resetForNewHand();

	}


	public CircularArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(CircularArrayList<Player> players) {
		Iterator<Player> it = players.getArrayListIterator();
		while(it.hasNext())
			it.next().setGame(this);
		this.players = players;
	}

	public List<Player> getSpectators() {
		return spectators;
	}

	public synchronized void addSpectator(Player player) {
		this.spectators.add(player);
	}
	public synchronized void removeSpectator(Player player) {
		this.spectators.remove(player);
	}
	public List<Card> getTableCards() {
		return tableCards;
	}
	
	private synchronized void addPlayers() {
		Iterator<Player> it = spectators.iterator();
		while(it.hasNext()){
			Player player = it.next();
			if (player.total_chips>MINIMUM_CHIPS){
				players.add(player);
				player.setSpectator(false);
				it.remove();
				NotificationManager.getInstance().notifyAll(new NewPlayerEvent(this, player));
			}
			
			if (players.size()>=MAX_NUMBER_OF_PLAYERS) break;
		}
		
	}

	private void distributePots() {
		bm.endHand(winners,button_player);
	}

	private List<List<Player>> getOrderedWinners() {
		ShowdownAnalyzer s = new ShowdownAnalyzer(this.getTableCards(),this.getShowdownPlayers());
		NotificationManager.getInstance().notifyAll(new ShowEvent(this,s.getOrderedHands()));
		return s.getWinners();
	}

	private void resetForNewHand() {
		
		// TODO remove players' last actions
		
		bm = new BetManager(this);
		button_player++;
		tableCards = new ArrayList<Card>();
		winners = null;
		
		// players with no chips move to spectators
		// remove players with LEAVE action
		removeLeaving(players.getArrayListIterator());
		removeLeaving(spectators.iterator());
	}


	private void removeLeaving(Iterator<Player> player_it) {
		while(player_it.hasNext()){
			Player p = player_it.next();
			if (p.getLastPlayerAction() ==null) continue;
			if (p.getLastPlayerAction().getAction()==PlayerAction.ABSENT)
				player_it.remove();
		}
		
	}

	private void dealAndBet() throws InconsistentStateException {
		deal();
		if (preFlopBettingRound()) return;
		flop();
		if (secondBettingRound()) return;
		fourthStreet();
		if (thirdBettingRound()) return;
		fifthStreet();
		if (fourthBettingRound()) return;
	}

	private void showdown() {
		winners =getOrderedWinners();
		if (bm.getShowdownPlayers().size()==1) return;
		
	}

	private boolean fourthBettingRound() throws InconsistentStateException {
		return bm.postFlopBettingRound(button_player);
	}

	private void fifthStreet() {
		tableCards.add(deck.dealOne());
		NotificationManager.getInstance().notifyAll(new FlopEvent(this));
	}

	private boolean thirdBettingRound() throws InconsistentStateException {
		return bm.postFlopBettingRound(button_player);
		
	}

	private void fourthStreet() {
		tableCards.add(deck.dealOne());
		NotificationManager.getInstance().notifyAll(new FlopEvent(this));
	}

	private boolean secondBettingRound() throws InconsistentStateException {
		return bm.postFlopBettingRound(button_player);
	}

	private void flop() {
		tableCards = new ArrayList<Card>();
		for (int i=0;i<3; i++)
			tableCards.add(deck.dealOne());
		NotificationManager.getInstance().notifyAll(new FlopEvent(this));
	}

	private boolean preFlopBettingRound() throws InconsistentStateException {
		bm.intialize();
		return bm.preFlop(button_player);
	}

	private void deal() {
		deck.shuffle();
		for (int i=0; i< players.size();i++){
			players.get(button_player+i).setCards(deck.dealOne(),deck.dealOne());
		}
	}

	public int getCurrentStake() {
		
		return bm.getCurrentstake();
	}

	public ArrayList<Player> getShowdownPlayers() {
		return bm.getShowdownPlayers();
	}
	public List<Pot> getSidePots(){
		return bm.getSidePots();
	}
	public Pot getPot(){
		return bm.getPot();
	}
	
}
