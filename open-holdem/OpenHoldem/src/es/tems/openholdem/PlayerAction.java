/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

public class PlayerAction {
	
	public static final short FOLD = 1;
	public static final short RAISE = 2;
	public static final short CALL = 3;
	public static final short ALL_IN_CALL = 4;
	public static final short ALL_IN_RAISE= 5;
	public static final short SMALL_BLIND= 6;
	public static final short BIG_BLIND= 7;
	public static final short CHECK = 8;
	public static final short ABSENT = 9;
	public static final short JUSTLEFT = 10;
	
	Player player;
	short action;
	int amount;
	public PlayerAction(Player player, short action, int amount) {
		super();
		this.player = player;
		this.action = action;
		this.amount = amount;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public int getAction() {
		return action;
	}
	public void setAction(short action) {
		this.action = action;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public String getActionString(){
		String text="";
		switch (action) {
		case PlayerAction.ALL_IN_RAISE:
			text ="All In";
			break;
		case PlayerAction.ALL_IN_CALL:
			text ="All In";
			break;
		case PlayerAction.CALL:
			text ="Call";
			break;
		case PlayerAction.CHECK:
			text ="Check";
			break;
		case PlayerAction.FOLD:
			text ="Fold";
			break;
		case PlayerAction.SMALL_BLIND:
			text ="Small Blind";
			break;
		case PlayerAction.BIG_BLIND:
			text ="Big Blind";
			break;
		case PlayerAction.RAISE:
			text ="Raise "+this.getAmount();
			break;
		case PlayerAction.ABSENT:
			text ="Left";
			break;
		case PlayerAction.JUSTLEFT:
			text ="left the table";
			break;
		}
		return text;
	}
	
	
}
