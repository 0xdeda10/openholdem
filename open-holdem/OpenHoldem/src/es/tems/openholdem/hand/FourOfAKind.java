/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Set;

import es.tems.openholdem.Player;

public class FourOfAKind extends NOfAKind {

	
	
	public FourOfAKind(Player player, Set<Card> tablecards) {
		super(player, tablecards, 4);
		checkForHand();
	}

//	public boolean equals(Object o){
//		if (!o.getClass().equals(FourOfAKind.class)) return false;
//		return (FourOfAKind.compare(this,(FourOfAKind) o)==0);
//	}
//	
//	public static int compare(FourOfAKind h1,FourOfAKind h2) {
//		int c = h1.getKindCard().compareTo(h2.getKindCard());
//		if (c!=0) return c;
//		else return h1.getKicker().compareTo(h2.getKicker());
//	}
//
//	private Card getKicker() {
//		return getKickers().get(0);
//	}

	
}
