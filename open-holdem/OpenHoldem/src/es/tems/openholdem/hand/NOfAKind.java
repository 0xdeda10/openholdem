/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.tems.openholdem.Player;

public class NOfAKind extends Hand{

	int n;
	int kind;

	public NOfAKind(Player player, Set<Card> tablecards, int n) {
		super(player,tablecards);
		this.n=n;
	}
	
	private boolean isNOfAKind(Set<Card> hand,int n) {
		int[] kind_ocurrences = getKind(getKindOcurrences(hand));
		return (kind_ocurrences[1]==n);	
	}
	
	protected Card getKindCard() {
		if(!hasHand()) return null;
		for(Card c: getFivecards())
			if (c.num==kind) return c;
		return null;
	}

	protected List<Card> getKickers() {
		if(!hasHand()) return null;
		return getKickers(getFivecards(),kind);
	}
	
	@Override
	protected boolean isHand(Set<Card> hand) {
		return isNOfAKind(hand, n);
	}
	
	@Override
	public int compare(Set<Card> set1, Set<Card> set2) {
		int[] kind_ocurrences_1= getKind(getKindOcurrences(set1));
		int[] kind_ocurrences_2 = getKind(getKindOcurrences(set2));
		int c = kind_ocurrences_1[0]- kind_ocurrences_2[0];
		if (c!=0) return c;
		
		List<Card> kickers1 = getKickers(set1,kind_ocurrences_1[0]);
		
		List<Card> kickers2 = getKickers(set2,kind_ocurrences_2[0]);
		
		// with both ordered sets of kickers, compare.
		
		for(int i=0; i< kickers1.size(); i++){
			c = kickers1.get(i).compareTo(kickers2.get(i));
			if (c!=0) return c;
		}
		return 0;
	}
	
	public static int[] getKind(Map<Integer, Integer> kinds){
		int[] kind_ocurrences =new int[2];
		int max=0,a=0,kind=-1;
		for(int num: kinds.keySet()){
			a = kinds.get(num);
			if (a>max){
				max=a;
				kind=num;
			}
		}
		kind_ocurrences[0]=kind;
		kind_ocurrences[1]=max;
		return kind_ocurrences;
	}
	public static List<Card> getKickers(Set<Card> hand,int kind){
		List<Card> kickers = new ArrayList<Card>();
		for(Card c: hand)
			if (c.num!=kind) kickers.add(c);
		Collections.sort(kickers);
		return kickers;
	}
	
	// Map (kind,ocurrences)
	protected static Map<Integer,Integer> getKindOcurrences(Set<Card> hand){
		Map<Integer,Integer> noOfKinds = new HashMap<Integer,Integer>();
		int ocurrences;
		for (Card c: hand){
			if (!noOfKinds.containsKey((int)c.num))
				noOfKinds.put((int) c.num, 1);
			else {
				ocurrences= noOfKinds.get((int)c.num);
				ocurrences++;
				noOfKinds.put((int) c.num, ocurrences);
			}
		}
		return noOfKinds;
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		String s="";
		switch (this.n){
		case 2: s= "Pair of ";
				break;
		case 3: s="Three ";
				break;
		case 4: s="Four ";
		}
		return s+FrenchCard.numToString((short) this.kind)+"'s with "+getKickers()+" kickers";
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {
		int[] kind_ocurrences=getKind(getKindOcurrences(this.fivecards));
		this.kind = kind_ocurrences[0];
	}
}
