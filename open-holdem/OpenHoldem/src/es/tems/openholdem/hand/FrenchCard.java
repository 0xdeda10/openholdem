/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

public class FrenchCard extends Card {
	
	public FrenchCard(short num, short suit) {
		super(num, suit);
	}
	public static final short DIAMONDS=0;
	public static final short HEARTS=1;
	public static final short SPADES=2;
	public static final short CLUBS=3;
	
	public static final short ACE=14;
	
	public static String toString(FrenchCard c){
		String s = numToString(c.num);
		s=s+ suitToString(c.suit);
		return s;
	}

	@Override
	public String toString() {
		return toString(this);
	}
	
	public static String numToString(short num){
		String s="";
		switch (num){

		case 11: 	s="J";
					break;
		case 12:	s="Q";
					break;
		case 13:	s="K";
					break;
		case 14: 	s="A";
					break;
		default: 	s=Short.toString(num);
		}
		return s;
	}
	public static String suitToString(short suit){
		String s="";
		switch (suit) {
		case DIAMONDS:	return s="\u2666";
		case CLUBS:		return s="\u2663";
		case HEARTS:	return s="\u2665";
		case SPADES:	return s="\u2660";	

		}
		return s;
	}
}
