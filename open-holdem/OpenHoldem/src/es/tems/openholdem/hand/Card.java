/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

public abstract class Card implements Comparable<Card>{
	
	protected short num;
	protected short suit;
	public short getNum() {
		return num;
	}
	public void setNum(short num) {
		this.num = num;
	}
	public short getSuit() {
		return suit;
	}
	public void setSuit(short suit) {
		this.suit = suit;
	}
	public Card(short num, short suit) {
		super();
		this.num = num;
		this.suit = suit;
	}
	
	public int compareTo(Card c){
		return this.num-c.num;
	}
	
	@Override
	public boolean equals(Object object){
		if (object instanceof Card){
			Card c =( Card) object;
			return ((c.num == this.num)&&(c.suit==this.suit));
		} else return false;
	}
	@Override
	public int hashCode(){
		return suit*100000+num;
		
	}
	public abstract String toString();
		
	
}
