/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Comparator;

/**
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class StrongerToWeakerHandComparator implements Comparator<Hand> {

	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 * 
	 * Orders from stronger hand to weaker hand
	 */
	@Override
	public int compare(Hand h1, Hand h2){
		Class<? extends Hand> c = h1.getClass();
		if (!c.equals(h2.getClass())) throw new ClassCastException();
		//Only compares two hands of the same class
		return -h1.compareTo(h2);
	};

}
