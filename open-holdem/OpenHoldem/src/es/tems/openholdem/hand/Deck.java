/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

public class Deck {

	protected Card[] cardarray;
	protected short size;
	
	protected short nextcard=0;
	
	public void shuffle(){
		shuffle(this);
		nextcard=0;
	}
	
	private static void shuffle(Deck deck) {
		int N = deck.size();
        for (int i = 0; i < N; i++) {
            // choose index uniformly in [i, N-1]
            int r = i + (int) (Math.random() * (N - i));
            Card swap = deck.cardarray[r];
            deck.cardarray[r] = deck.cardarray[i];
            deck.cardarray[i] = swap;
        }
		
	}

	private int size() {
		return size;
	}

	public Card dealOne(){
		if (nextcard<size)
			return this.cardarray[nextcard++];
		else throw new IndexOutOfBoundsException();
	}
	public String toString(){
		String s="";
		for(int i=0; i<this.cardarray.length; i++)
			s=s+cardarray[i].toString()+"\n";
		return s;
	}
}
