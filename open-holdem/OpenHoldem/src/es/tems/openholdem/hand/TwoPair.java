/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import es.tems.openholdem.Player;

/**
 * 
 * Represents and detects the Two Pair hand
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class TwoPair extends Hand  {

	private TwoPairKinds twoPairKinds;

	/**
	 * @param player The player who might have this hand
	 * @param tablecards The cards on the table
	 */
	public TwoPair(Player player, Set<Card> tablecards) {
		super(player, tablecards);
		checkForHand();
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		TwoPairKinds kinds1 = getTwoPairKinds(hand1);
		TwoPairKinds kinds2 = getTwoPairKinds(hand2);
		if (kinds1.kindOfPair1!=kinds2.kindOfPair1)
			return (kinds1.kindOfPair1-kinds2.kindOfPair1);
		else if (kinds1.kindOfPair2!=kinds2.kindOfPair2)
			return (kinds1.kindOfPair2-kinds2.kindOfPair2);
		else return (kinds1.kindOfKicker-kinds2.kindOfKicker);
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#isHand(java.util.Set)
	 */
	@Override
	protected boolean isHand(Set<Card> hand) {
		TwoPairKinds twoPairKinds = getTwoPairKinds(hand);
		return twoPairKinds.isTwoPair;
	}
	
	protected TwoPairKinds getTwoPairKinds(Set<Card> hand) {
		boolean alreadyTwo = false;
		Map<Integer, Integer> kindOcurrences = NOfAKind.getKindOcurrences(hand);
		Iterator<Integer> key_it = kindOcurrences.keySet().iterator();
		Integer occurrences,kind;
		TwoPairKinds  result = new TwoPairKinds();
		result.isTwoPair=false;
		while (key_it.hasNext()) {
			kind=key_it.next();
			occurrences =  kindOcurrences.get(kind);
			if (occurrences == 2){
				if (!alreadyTwo){
					alreadyTwo = true;
					result.kindOfPair1=kind;
				}else{
					result.kindOfPair2=kind;
					result.isTwoPair=true;
					sortKinds(result);
					return result;
				}
			} else result.kindOfKicker = kind;
		}
		return result;

	}
	
	/**
	 * @param result
	 */
	private void sortKinds(TwoPairKinds kinds) {
		if (kinds.kindOfPair1<kinds.kindOfPair2){
			int temp = kinds.kindOfPair2;
			kinds.kindOfPair2=kinds.kindOfPair1;
			kinds.kindOfPair1 = temp;
		}
	}

	private class TwoPairKinds{
		boolean isTwoPair;
		int kindOfPair1;
		int kindOfPair2;
		int kindOfKicker;
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Two Pair of "
				+ FrenchCard
						.numToString((short) this.twoPairKinds.kindOfPair1)
				+ "'s and "
				+ FrenchCard
						.numToString((short) this.twoPairKinds.kindOfPair2)
				+ "'s with "
				+ FrenchCard
				.numToString((short) this.twoPairKinds.kindOfKicker)
				+ " kicker";
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {
		twoPairKinds = getTwoPairKinds(this.fivecards);
	}


}
