/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Set;

import es.tems.openholdem.Player;

public class StraightFlush extends Hand {
	
	
	public StraightFlush(Player player, Set<Card> tablecards) {
		super(player,tablecards);
		checkForHand();
	}
	
	

//	public boolean equals(Object sf){
//		if (!sf.getClass().equals(StraightFlush.class)) return false;
//		return (StraightFlush.compare(this,(StraightFlush) sf)==0);
//	}
	
	// returns true if sf1 is a better straight flush hand than sf2
	// make this method in hand
//	public static int compare(StraightFlush sf1,StraightFlush sf2) {
//		return (sf1.getHighestCard().compareTo(sf2.getHighestCard()));
//	}



	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		return Straight.getHighStraight(hand1).num - Straight.getHighStraight(hand2).num;
	}



	@Override
	protected boolean isHand(Set<Card> hand) {
		//is hand a flush? Is it a Straight ?
		return Flush.isFlush(hand)&&Straight.isStraight(hand);
	}



	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return FrenchCard.numToString(getHighestCard().getNum())+"-high Straight Flush";
	}



	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {}

}
