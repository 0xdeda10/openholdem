/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Iterator;
import java.util.Set;

import es.tems.openholdem.Player;

public class Flush extends Hand {


	public Flush(Player player, Set<Card> tablecards) {
		super(player, tablecards);
		checkForHand();
	}

	public boolean isHand(Set<Card> hand) {
		return isFlush(hand);		
	}

	public static boolean isFlush(Set<Card> hand) {
		Iterator<Card> it = hand.iterator();
		if (!it.hasNext())return false;
		Card card = it.next();

		short suit=card.suit;
		while(it.hasNext()){
			card=it.next();
			if (card.suit!=suit) return false;
		}
		return true;
		
	}

	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		
		return HighestCard.compareStatic(hand1, hand2);
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return FrenchCard.numToString(getHighestCard().getNum())+"-high Flush";
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {}
	
}
