/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

public class FrenchDeck extends Deck {

	
	public static final short CARDSPERSUIT=13;
	public static final short DECKSIZE=52;

	public FrenchDeck(){
		this.size=DECKSIZE;
		//fill in deck
		this.cardarray= new FrenchCard[DECKSIZE];
		int n=0;
		for (short i=2;i<=CARDSPERSUIT+1;i++)
			for(short j=0;j<4;j++){
				this.cardarray[n]=new FrenchCard(i,j);
				n++;
			}
	}

	
	
}
