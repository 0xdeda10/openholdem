/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import es.tems.openholdem.Player;

public class Straight extends Hand {

	public Straight(Player player, Set<Card> tablecards) {
		super(player,tablecards);
		checkForHand();
	}


	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		return getHighStraight(hand1).num - getHighStraight(hand2).num;
	}

	public static Card getHighStraight(Set<Card> hand1) {
		List<Card> cardlist1 = new ArrayList<Card>(hand1);
		Collections.sort(cardlist1);
		Card high = cardlist1.get(cardlist1.size()-1);
		if (high.num!=FrenchCard.ACE) 
			return high;
		Card high2 = cardlist1.get(cardlist1.size()-2);
		if (high2.num==13)
			return high;
		else return high2;
	}


	@Override
	protected boolean isHand(Set<Card> hand) {
		return isStraight(hand);
	}


	public static boolean isStraight(Set<Card> hand) {
		List<Card> cardlist = new ArrayList<Card>(hand);
		Collections.sort(cardlist);
		Card c =cardlist.get(0);
		short lastnum = c.num;
		boolean checkforAce=false;
		if (lastnum==2) checkforAce=true;
		for(int i=1; i<5;i++){
			c = cardlist.get(i);
			if (checkforAce && i==4 && c.num==FrenchCard.ACE){
				break;
			}
			lastnum++;
			if (c.num!=lastnum) return false;
			
		} 
		return true;
	}


	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return FrenchCard.numToString(getHighestCard().getNum())+"-high Straight";
	}


	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {}

}
