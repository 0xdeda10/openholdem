/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import es.tems.openholdem.Player;
import es.tems.util.Combinatorial;

public abstract class Hand implements Comparator<Set<Card>>{
	Player player;


	Set<Card> tcards;
	Set<Card> fivecards;

	boolean hasHand;
	
	public Hand(Player player, Set<Card> tablecards) {
		this.player=player;
		this.tcards=tablecards;
	}
	
	
	public Player getPlayer() {
		return player;
	}
	public Set<Card> getFivecards() {
		return fivecards;
	}
	
	public Card getHighestCard() {
		return getHighestCard(fivecards);
	}
	
	public Set<Set<Card>> getFiveCardCombinations(){

		Set<Card> sevencards = new HashSet<Card>();
		sevencards.add(this.player.getCards()[0]);
		sevencards.add(this.player.getCards()[1]);
		sevencards.addAll(tcards);
		Combinatorial<Card> c = new Combinatorial<Card>(sevencards);
		return c.getCombinations(5);
		
	}
	
	
	public boolean hasHand() {
		return hasHand;
	}
	
	protected void checkForHand(){
		//gets all possible five card combinations and checks if they are 
		// a hand of this type (based on isHand implementation).
		// if they are then get the best combination and set fivecards member.
		hasHand= false;
		Set<Set<Card>> combinations = this.getFiveCardCombinations();
		List<Set<Card>> hands = new ArrayList<Set<Card>>();
		for(Set<Card> hand: combinations)
			if (isHand(hand)){
				hands.add(hand);
				hasHand= true;
			}
		if (hasHand){
			Collections.sort(hands, this);
			fivecards=hands.get(hands.size()-1);
			settlehand();
		}
	}
	



	public int compareTo(Hand hand){
		return this.compare(this.fivecards, hand.fivecards);
	}
	
	//Abstract methods
	
	/**
	 * Check if this set of cards is actually the hand represented by the implementation
	 * 
	 * @param hand
	 * @return true if this set of cards is actually the hand represented by the implementation
	 */
	protected abstract boolean isHand(Set<Card> hand) ;
	
	/**
	 * To be called if this hand exists in the set and after fivecards contain the highest value of this hand
	 */
	protected abstract void settlehand();
	
	
	// Static methods
	
	public static List<List<Hand>> sort(List<Hand> hands) {
		// Takes into account tied hands
		List<List<Hand>> sortedhands = new ArrayList<List<Hand>>();
		if (hands.size()<2) {
			sortedhands.add(hands);
			return sortedhands;
		}
		Comparator<Hand> comparator = new StrongerToWeakerHandComparator(); 
		Collections.sort(hands, comparator);
		Iterator<Hand> it = hands.iterator();
		Hand h1 = it.next();
		Hand h2;
		List<Hand> tiedhands = null;
		boolean foundequals = false;

		while (it.hasNext()) {
			h2 = it.next();
			if (comparator.compare(h1, h2) == 0) {

				if (!foundequals) {
					tiedhands = new ArrayList<Hand>();
					tiedhands.add(h1);
					foundequals = true;
				} else
					tiedhands.add(h1);

			} else {


				if (foundequals) {
					foundequals = false;
				} else {
					tiedhands = new ArrayList<Hand>();
				}
				tiedhands.add(h1);
				sortedhands.add(tiedhands);
			}
			h1 = h2;
		}
		if (!foundequals) tiedhands = new ArrayList<Hand>();
		tiedhands.add(h1);
		sortedhands.add(tiedhands);
		return sortedhands;

	}
	
	public static Card getHighestCard(Set<Card> set){
		List<Card> list = new ArrayList<Card>(set);
		Collections.sort(list);
		return list.get(list.size()-1);
//		Iterator<Card> it = set.iterator();
//		Card highest=it.next();
//		Card c;
//		while (it.hasNext()){
//			c = it.next();
//			if (c.compareTo(highest)>0) highest = c;
//		}
//		return highest;
	}

	@Override
	public String toString(){
		return this.getClass().getSimpleName()+":"+player.getNick()+":"+fivecards.toString();
		
	}

	public abstract String getDescription();
	
	
}
