/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import es.tems.openholdem.Player;

public class FullHouse extends Hand {

	private FullHouseKinds fullHouseKinds;

	public FullHouse(Player player, Set<Card> tablecards) {
		super(player, tablecards);
		checkForHand();
	}

	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		FullHouseKinds fullHouseKinds1 = getFullHouseKinds(hand1);
		FullHouseKinds fullHouseKinds2 = getFullHouseKinds(hand2);
		int result = fullHouseKinds1.kindOfThree - fullHouseKinds2.kindOfThree;
		if (result != 0)
			return result;
		else
			return (fullHouseKinds1.kindOfPair - fullHouseKinds2.kindOfPair);

	}

	@Override
	protected boolean isHand(Set<Card> hand) {
		FullHouseKinds fullHouseKinds = getFullHouseKinds(hand);
		return fullHouseKinds.isFullHouse;
	}

	protected FullHouseKinds getFullHouseKinds(Set<Card> hand) {
		boolean three = false;
		boolean two = false;
		Map<Integer, Integer> kindOcurrences = NOfAKind.getKindOcurrences(hand);
		Iterator<Integer> key_it = kindOcurrences.keySet().iterator();
		Integer occurrences, kind;
		FullHouseKinds result = new FullHouseKinds();
		result.isFullHouse = false;
		while (key_it.hasNext()) {
			kind = key_it.next();
			occurrences = kindOcurrences.get(kind);
			if (occurrences == 3){
				three = true;
				result.kindOfThree = kind;
			}
			if (occurrences == 2){
				two = true;
				result.kindOfPair = kind;
			}
			if (two && three) {
				result.isFullHouse = true;
				return result;
			}
		}
		return result;

	}

	private class FullHouseKinds {
		boolean isFullHouse;
		int kindOfThree;
		int kindOfPair;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Full House of "
				+ FrenchCard
						.numToString((short) this.fullHouseKinds.kindOfThree)
				+ "'s and "
				+ FrenchCard
						.numToString((short) this.fullHouseKinds.kindOfPair)
				+ "'s";
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {
		fullHouseKinds = getFullHouseKinds(this.fivecards);
	}

}
