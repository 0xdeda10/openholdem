/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.hand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import es.tems.openholdem.Player;

/**
 *  The simplest Hand of all (all hands are HighestCard)
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class HighestCard extends Hand {

	/**
	 * Constructor
	 * 
	 * @param player
	 * @param tablecards
	 */
	public HighestCard(Player player, Set<Card> tablecards) {
		super(player, tablecards);
		checkForHand();
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Set<Card> hand1, Set<Card> hand2) {
		return HighestCard.compareStatic( hand1, hand2);
		
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#isHand(java.util.Set)
	 */
	@Override
	protected boolean isHand(Set<Card> hand) {
		return true;
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#getDescription()
	 */
	@Override
	public String getDescription() {
		return FrenchCard.numToString(getHighestCard().getNum())+" Highest Card";
	}

	/* (non-Javadoc)
	 * @see es.tems.openholdem.hand.Hand#settlehand()
	 */
	@Override
	protected void settlehand() {}

	
	public static int compareStatic(Set<Card> hand1, Set<Card> hand2){
		List<Card> list1 = new ArrayList<Card>(hand1);
		List<Card> list2 = new ArrayList<Card>(hand2);
		Collections.sort(list1);
		Collections.sort(list2);
		int result=0;
		int i=1;
		int size=list1.size();
		while(result==0 && i<=size){
			result = list1.get(size-i).compareTo(list2.get(size-i));
			i++;
		}
		return result;
	}
}
