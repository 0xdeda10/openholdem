/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import es.tems.openholdem.event.NotificationManager;
import es.tems.openholdem.event.PlayerActionEvent;
import es.tems.openholdem.event.PotEvent;
import es.tems.openholdem.exception.InconsistentStateException;
import es.tems.util.CircularArrayList;

/**
 * Class that implements the logic of betting: manages betting rounds and pots
 * 
 * Further information:
 * http://poker.stackexchange.com/questions/462/how-are-side-pots-built
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 */
public class BetManager {
	public static final short DEFAULT_BIGBLIND = 4;
	public static final short DEFAULT_SMALLBLIND = 2;
	private List<Pot> sidepots = new ArrayList<Pot>();
	private int currentstake;
	private Player last_word_player;
	private Pot pot;
	CircularArrayList<Player> allplayers;

	private Game game;
	private boolean allIns = false;

	public BetManager(Game game) {
		super();
		this.game = game;

	}

	public void intialize() {
		Iterator<Player> it = this.getPlayers().getArrayListIterator();
		while (it.hasNext())
			it.next().reset();
		this.pot = new Pot(0, new CircularArrayList<Player>(getPlayers()), "Main");
	}

	public int getCurrentstake() {
		return currentstake;
	}

	private CircularArrayList<Player> getPlayers() {
		return game.getPlayers();
	}

	/**
	 * Initiates the betting rounds before Flop
	 * 
	 * @param button_player
	 * @return
	 * @throws InconsistentStateException
	 */
	public boolean preFlop(short button_player) throws InconsistentStateException {
		allplayers = this.getPlayers();

		Player betting_player;
		PlayerAction pa;
		if (allplayers.size() > 2) {
			betting_player = allplayers.get(button_player + 1);
			pa = betting_player.betSmallBlind(DEFAULT_SMALLBLIND);
			currentstake = DEFAULT_SMALLBLIND;
			NotificationManager.getInstance().notifyAll(new PlayerActionEvent(game, pa));
			betting_player = allplayers.get(button_player + 2);
			pa = betting_player.betBigBlind(DEFAULT_BIGBLIND);
			last_word_player = betting_player;
			currentstake = DEFAULT_BIGBLIND;
			NotificationManager.getInstance().notifyAll(new PlayerActionEvent(game, pa));
			return bettingRounds(button_player + 3);
		} else {
			betting_player = allplayers.get(button_player);
			pa = betting_player.betSmallBlind(DEFAULT_SMALLBLIND);
			currentstake = DEFAULT_SMALLBLIND;
			NotificationManager.getInstance().notifyAll(new PlayerActionEvent(game, pa));
			betting_player = allplayers.get(button_player + 1);
			pa = betting_player.betBigBlind(DEFAULT_BIGBLIND);
			last_word_player = betting_player;
			currentstake = DEFAULT_BIGBLIND;
			NotificationManager.getInstance().notifyAll(new PlayerActionEvent(game, pa));
			return bettingRounds(button_player + 2);
		}
	}

	/**
	 * 
	 * Initiates the betting rounds after Flop
	 * 
	 * @param button_player
	 * @return
	 * @throws InconsistentStateException
	 */
	public boolean postFlopBettingRound(short button_player) throws InconsistentStateException {
		CircularArrayList<Player> players = this.getPlayers();
		Player betting_player = getNextActivePlayer(players.get(button_player));
		last_word_player = getPreviousActivePlayer(players.get(button_player + 1));
		return bettingRounds(players.indexOf(betting_player));

	}

	/**
	 * Ends a hand once the winners have been sorted, distributing pots
	 * 
	 * @param winners
	 *            a list of lists of Players ordered by hand strength. Players
	 *            in the same List are tied
	 * @param button_player
	 */
	public void endHand(List<List<Player>> winners, short button_player) {
		for (List<Player> tiedPlayers : winners) {
			for (Player player : tiedPlayers) {
				pot.setAmount(pot.getAmount() + player.getTablechips());
				player.setTablechips(0);
			}
		}
		// Distribute main pot
		distributePot(winners, pot, button_player);
		for (Pot sidepot : sidepots) {
			distributePot(winners, sidepot, button_player);
		}

	}

	/**
	 * 
	 * Implements betting rounds starting from player
	 * 
	 * @param starting_player
	 * @return true if only one active player remains after the betting rounds
	 * @throws InconsistentStateException
	 */
	private boolean bettingRounds(int starting_player) throws InconsistentStateException {
		Player betting_player;
		PlayerAction pa;
		CircularArrayList<Player> players = getPlayers();
		allIns = false;
		int i = 0;
		do {
			betting_player = players.get(starting_player + i);
			i++;
			if (betting_player.isInactive())
				continue;
			pa = betting_player.askForAction(currentstake, getPlayers());
			update(pa);

			// TODO this makes that last word players has no chance to ALLIN/CALL
			if (activeplayers() == 0) {
				if (allIns)
					createSidePots();
				return true;
			}
		} while (betting_player.getID() != last_word_player.getID());
		if (allIns)
			createSidePots();
		return false;
	}

	/**
	 * @param betting_player
	 */
	private void playerLeft(Player player) {
		updatePotsPlayerFolded(player);
		player.setLastPlayerAction(new PlayerAction(player, PlayerAction.ABSENT, 0));
	}

	private void update(PlayerAction pa) {

		if (pa.action == PlayerAction.RAISE) {
			last_word_player = getPreviousActivePlayer(pa.getPlayer());
			currentstake = currentstake + pa.getAmount();
		} else if (pa.action == PlayerAction.ALL_IN_RAISE) {
			last_word_player = getPreviousActivePlayer(pa.getPlayer());
			currentstake = currentstake + pa.getAmount();
			allIns = true;
		} else if (pa.action == PlayerAction.ALL_IN_CALL) {
			allIns = true;
		} else if (pa.getAction() == PlayerAction.FOLD) {
			updatePotsPlayerFolded(pa.getPlayer());
		} else if (pa.getAction() == PlayerAction.JUSTLEFT) {
			playerLeft(pa.getPlayer());
		}
		NotificationManager.getInstance().notifyAll(new PlayerActionEvent(game, pa));
	}

	/*
	 * Methods relating to active or showdown players
	 */
	// TODO Check for redundant code

	private int activeplayers() {
		Player p;
		CircularArrayList<Player> players = getPlayers();
		int n = players.size();
		for (int i = 0; i < players.size(); i++) {
			p = players.get(i);
			if (p.isInactive())
				n--;
		}
		return n;
	}

	private Player getPreviousActivePlayer(Player player) {
		Player p = null;
		CircularArrayList<Player> players = getPlayers();
		int index = players.indexOf(player);
		for (int i = 1; i < players.size(); i++) {
			p = players.get(index - i);
			if (!p.isInactive())
				break;
		}
		return p;
	}

	private Player getNextActivePlayer(Player player) {
		Player p = null;
		CircularArrayList<Player> players = getPlayers();
		int index = players.indexOf(player);
		for (int i = 1; i < players.size(); i++) {
			p = players.get(index + i);
			if (!p.isInactive())
				break;
		}
		return p;
	}

	public CircularArrayList<Player> getActiveplayers() {
		Player p;
		CircularArrayList<Player> players = getPlayers();
		CircularArrayList<Player> active_players = new CircularArrayList<Player>();
		for (int i = 0; i < players.size(); i++) {
			p = players.get(i);
			if (!p.isInactive())
				active_players.add(p);
		}
		return active_players;
	}

	public ArrayList<Player> getShowdownPlayers() {
		// Showdown players are all that have not Folded.
		Player p;
		CircularArrayList<Player> players = getPlayers();
		ArrayList<Player> showdown = new ArrayList<Player>();
		for (int i = 0; i < players.size(); i++) {
			p = players.get(i);
			if (p.hasActiveBet())
				showdown.add(p);
		}
		return showdown;
	}

	/*
	 * Pots management
	 */

	public Pot getPot() {
		return pot;
	}

	public List<Pot> getSidePots() {
		return this.sidepots;
	}

	/*
	 * manage all-ins by creating side pots
	 */
	private void createSidePots() {
		//TODO Check this code extensively
		
		// Get all different bets
		List<Integer> bets = new ArrayList<Integer>();
		List<Player> currentPotPlayers = pot.getPlayers();
		
		Player p;
		for (int k=0; k < currentPotPlayers.size(); k++) {
			p= currentPotPlayers.get(k);
			Integer tchips = p.getTablechips();
			if (!bets.contains(tchips))
				bets.add(tchips);
		}

		Collections.sort(bets);

		Iterator<Integer> bet_it = bets.iterator();
		Integer bet = bet_it.next();
		int mainpot = pot.getAmount();
		int i = 1;
		int previous_bet = 0;
		CircularArrayList<Player> players;
		while (bet_it.hasNext()) {
			// sidepot for each "bet"
			players = new CircularArrayList<Player>();
			for (int k=0; k < currentPotPlayers.size(); k++) {
				p= currentPotPlayers.get(k);
				if (p.getTablechips() >= bet)
					players.add(p);
			}
			sidepots.add(new Pot(mainpot + (bet - previous_bet)
					* players.size(), players, "Sidepot" + i));
			previous_bet = bet;
			bet = bet_it.next();
			i++;
			mainpot = 0;
		}
		// If the greatest bet includes all-ins
		boolean currentWithAllIns = false;
		players = new CircularArrayList<Player>();
		for (int k=0; k < currentPotPlayers.size(); k++) {
			p= currentPotPlayers.get(k);
			if (p.isAllIn())
				currentWithAllIns = true;
			if (p.getTablechips() >= bet)
				players.add(p);
		}
		if (currentWithAllIns) {
			sidepots.add(new Pot(mainpot + (bet - previous_bet)
					* players.size(), players, "Sidepot" + i));
			previous_bet = bet;
			pot = new Pot(0, getActiveplayers(), "Main");
		} else {
			pot = new Pot(0, getActiveplayers(), "Main");
		}
		int tc;
		for (int k=0; k < currentPotPlayers.size(); k++) {
			p= currentPotPlayers.get(k);
			tc = (p.getTablechips() >= previous_bet) ? (p.getTablechips() - previous_bet)
					: 0;
			p.setTablechips(tc);
		}
		allIns = false;
	}

	/*
	 * Manages the bets of folded players
	 */
	private void updatePotsPlayerFolded(Player player) {
		pot.setAmount(pot.getAmount() + player.getTablechips());
		player.setTablechips(0);
		pot.removePlayer(player);
		List<Player> players;
		for (Pot sidepot : sidepots) {
			players = sidepot.getPlayers();
			if (players.contains(player))
				players.remove(player);
		}
	}

	/*
	 * Distributes a Pot
	 */
	private void distributePot(List<List<Player>> orderedPlayers, Pot pot, short button_player) {
		List<Player> potPlayers = pot.getPlayers();
		List<Player> winners = new ArrayList<Player>();
		for (List<Player> tiedPlayers : orderedPlayers) {
			for (Player player : tiedPlayers) {
				// search player in pot, if it is there then
				// if tiedPlayers.size()>1 distribute, else assign.
				if (potPlayers.contains(player))
					winners.add(player);
			}
			if (winners.size() != 0) {
				int earnings = pot.getAmount() / winners.size();
				boolean tied = winners.size() > 1;
				for (Player player : winners) {
					player.setTotal_chips(player.getTotal_chips() + earnings);
					NotificationManager.getInstance()
							.notifyAll(new PotEvent(game, player, earnings, pot.getPotID(), tied));
				}
				int sparechips = pot.getAmount() % winners.size();
				if (sparechips != 0)
					distributeSpareChips(sparechips, winners, button_player);
				break;
			}
		}
	}

	private void distributeSpareChips(int sparechips, List<Player> winners, short button_player) {
		CircularArrayList<Player> players = this.getPlayers();
		int i = 1;
		while (sparechips > 0) {
			Player p = players.get(button_player + i);
			if (winners.contains(p)) {
				p.total_chips++;
				sparechips--;
			}
		}
	}

}
