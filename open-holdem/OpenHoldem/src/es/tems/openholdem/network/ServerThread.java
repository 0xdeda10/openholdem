/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import es.tems.openholdem.Game;
import es.tems.openholdem.Log;
import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerAction;

public class ServerThread extends Thread {

	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private Game game;
	private Log log = Log.getInstance();
	
	public ServerThread(Socket socket, Game game) {
		super();
        this.socket = socket;
        this.game = game;
	}
	
	public void run() {
		Player p=null;
		TextSocketUI ui = null;
		log.info("Player connected from " + socket.getInetAddress());
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(
                new InputStreamReader(
                    socket.getInputStream()));
            String inputLine, outputLine;
            ui = new TextSocketUI(this);
            ui.setServer(this);
            
            
            Login login = new Login(in,out,game,ui);
            p = login.getPlayer();
            game.addSpectator(p);
            //
            while ((inputLine = in.readLine()) != null) {
                outputLine = ui.processInput(inputLine);
                out.println(outputLine);
                if (outputLine.equals("Bye!")){
                    break;
                }
            }
            socket.close();
        } catch (IOException e) {
            	log.info("Failed to read from connection to " + socket.getInetAddress());
        } finally {
        	log.info("Player disconnected from " + socket.getInetAddress());
        	try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	out.close();
        }
    	if (ui != null) ui.endInterface();
    }
	
	public void processOutput(String outputLine){
		out.println(outputLine);
	}

}
