/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.tems.openholdem.hand.Card;
import es.tems.openholdem.Game;
import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerAction;
import es.tems.openholdem.Pot;

/**
 * 
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 * 
 */
public class TextScreen {
	private static final int SCREEN_WIDTH = 80;
	private static final int POT_WIDTH = 13;
	private List<String> notifications = new ArrayList<String>();
	private String prompt="Welcome!";
	private Card[] owncards;
	private Pot pot;
	private List<Pot> sidepots;
	private List<Card> tablecards;
	private List<Player> players;
	private boolean showdown;

	public List<String> getNotifications() {
		return notifications;
	}

	public void addNotifications(String line) {
		this.notifications.add(line);
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public Card[] getOwncards() {
		return owncards;
	}

	public void setOwncards(Card[] owncards) {
		this.owncards = owncards;
	}
	
	public Pot getPot() {
		return pot;
	}

	public void setPot(Pot pot) {
		this.pot = pot;
	}
	
	public List<Pot> getSidepots() {
		return sidepots;
	}

	public void setSidepots(List<Pot> sidepots) {
		this.sidepots = sidepots;
	}

	public List<Card> getTablecards() {
		return tablecards;
	}

	public void setTablecards(List<Card> tablecards) {
		this.tablecards = tablecards;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public boolean isShowdown() {
		return showdown;
	}

	public void setShowdown(boolean showdown) {
		this.showdown = showdown;
	}

	/**
	 * 
	 */
	public List<String> getLines() {
		List<String> lines = new ArrayList<String>();
		int playerWidth = 2 * SCREEN_WIDTH / Game.MAX_NUMBER_OF_PLAYERS - 1;
		// First Player Row
		String line1 = "";
		String line2 = "";
		String line3 = "";
		String line4 = "";

		String str, chips, lastaction, tablechips, prefix, cards;
		for (int i = 0; i < Game.MAX_NUMBER_OF_PLAYERS / 2; i++) {
			prefix = i == 0 ? " | " : "";
			if (i < players.size()) {
				str = players.get(i).getNick();
				chips = "Total: "
						+ String.valueOf(players.get(i).getTotal_chips());
				PlayerAction pa = players.get(i).getLastPlayerAction();
				if (pa != null)
					lastaction = pa.getActionString();
				else
					lastaction = "";
				
				cards = showdown ? "  "
						+ players.get(i).getCards()[0].toString() + " "
						+ players.get(i).getCards()[1].toString() : "";
				tablechips = prefix
						+ String.valueOf(players.get(i).getTablechips())
						+ cards;
			} else {
				str = "No player";
				chips = "";
				lastaction = "";
				tablechips = prefix;
			}
			line1 += pad(str, playerWidth);
			line2 += pad(chips, playerWidth);
			line3 += pad(lastaction, playerWidth);
			line4 += pad(tablechips, playerWidth);
			if (i < Game.MAX_NUMBER_OF_PLAYERS / 2 - 1) {
				line1 += "|";
				line2 += "|";
				line3 += "|";
				line4 += " ";
			} else
				line4 += "|";

		}
		lines.add(line1);
		lines.add(line2);
		lines.add(line3);
		lines.add("  _____________________________________________________________________________");
		lines.add(line4);

		String line5 = " |        ";
		String line6 = " |       ";
		String line7 = " |       ";
		String line8 = " |       ";

		for (int i = 0; i < tablecards.size(); i++) {
			line5 += "____  ";
			line6 += "|    |";
			line7 += "| " + pad(tablecards.get(i).toString(), 3) + "|";
			line8 += "|____|";
		}

		lines.add(pad(line5, SCREEN_WIDTH - 1) + "|");
		if (pot!=null)
			line6 += "Pot :"+ pot.getAmount();
		if (sidepots != null) {
			for (int i = 0; i < sidepots.size(); i++) {
				String potStr = pad("Pot" + (i+1) + ":"
						+ sidepots.get(i).getAmount(), POT_WIDTH);
				switch (i % 3) {
				case 0:
					line7 += potStr;
					break;
				case 1:
					line8 += potStr;
					break;
				case 2:
					line6 += potStr;
				}

			}
		}
		lines.add(pad(line6, SCREEN_WIDTH - 1) + "|");
		lines.add(pad(line7, SCREEN_WIDTH - 1) + "|");
		lines.add(pad(line8, SCREEN_WIDTH - 1) + "|");

		String line9 = "";
		String line11 = "";
		String line12 = "";
		String line13 = "";
		for (int i = Game.MAX_NUMBER_OF_PLAYERS / 2 + 1; i <= Game.MAX_NUMBER_OF_PLAYERS; i++) {
			prefix = i == (Game.MAX_NUMBER_OF_PLAYERS / 2 + 1) ? " | " : "";
			if (i < players.size()) {
				str = players.get(i).getNick();
				chips = "Total: "
						+ String.valueOf(players.get(i).getTotal_chips());
				lastaction = players.get(i).getLastPlayerAction()
						.getActionString();
				
				cards = showdown ? "  "
						+ players.get(i).getCards()[0].toString() + " "
						+ players.get(i).getCards()[1].toString() : "";
				tablechips = prefix
						+ String.valueOf(players.get(i).getTablechips())
						+ cards;
			} else {
				str = "No player";
				chips = "";
				lastaction = "";
				tablechips = prefix;
			}

			line9 += pad(tablechips, playerWidth);
			line11 += pad(lastaction, playerWidth);
			line12 += pad(str, playerWidth);
			line13 += pad(chips, playerWidth);

			if (i < Game.MAX_NUMBER_OF_PLAYERS) {
				line11 += "|";
				line12 += "|";
				line13 += "|";
				line9 += " ";
			} else
				line9 += "|";

		}
		lines.add(line9);
		lines.add(" |_____________________________________________________________________________|");
		lines.add(line11);
		lines.add(line12);
		lines.add(line13);
		lines.add("  ____   ____       __________________________________________________________");
		String line14 = " |    | |    |     |";
		String line15;
		if (owncards[0] != null && owncards[1] != null  )
			line15 = " | " + pad(owncards[0].toString(), 3) + "| | "
					+ pad(owncards[1].toString(), 3) + "|     |";
		else
			line15 = " |    | |    |     |";

		String line16 = " |____| |____|     |";
		String line17 = "                   |";
		String line18 = "                   |";

		// Fill in notifications in lines 14,15,16,17,18
		int not_size = notifications.size();
		int num_lines = (not_size>5) ? 5:not_size;
		switch(num_lines){
		case 5: line14 += notifications.get(not_size - 5);
		case 4: line15 += notifications.get(not_size - 4);
		case 3: line16 += notifications.get(not_size - 3);
		case 2: line17 += notifications.get(not_size - 2);
		case 1: line18 += notifications.get(not_size - 1);
		}
		
		
		
		
		

		lines.add(pad(line14, SCREEN_WIDTH));
		lines.add(pad(line15, SCREEN_WIDTH));
		lines.add(pad(line16, SCREEN_WIDTH));
		lines.add(pad(line17, SCREEN_WIDTH));
		lines.add(pad(line18, SCREEN_WIDTH));
		// Fill in Prompt
		lines.add(pad(prompt, SCREEN_WIDTH));
		return lines;

	}



	private String pad(String info, int width) {
		String str;
		if (info.length() < width) {
			char[] array = new char[width - info.length()];
			Arrays.fill(array, ' ');
			str = new String(array);
			return info + str;
		} else
			return info.substring(0, width);

	}
}
