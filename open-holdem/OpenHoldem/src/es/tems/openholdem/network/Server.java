/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.network;

import java.io.IOException;
import java.net.ServerSocket;

import es.tems.openholdem.Game;

public class Server implements Runnable{

	private static final int DEFAULT_PORT = 60000;
	int portNumber = DEFAULT_PORT;
	Game game;
	
	
	public Server(Game game) {
		super();
		this.game=game;
	}
	
	public Server(Game game,int portNumber) {
		super();
		this.game=game;
		this.portNumber = portNumber;
	}
	
	@Override
	public void run() {
		
        boolean listening = true;
         
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) { 
            while (listening) {
                new ServerThread(serverSocket.accept(),game).start();
            }
        } catch (IOException e) {
        	//TODO Auto-generated catch block
            e.printStackTrace();
        }
		
	}

}
