/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import es.tems.openholdem.Game;
import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerUI;

/**
 * @author Álvaro Ramos (alvaro@pimedios.com)
 *
 */
public class Login {

	private static final String BANNER="\n\n\n\n\n\n\n\n\n\n   ____                     _    _       _     _                \n"+
"  / __ \\                   | |  | |     | |   | |\n"+               
" | |  | |_ __   ___ _ __   | |__| | ___ | | __| | ___ _ __ ___  \n"+
" | |  | | '_ \\ / _ \\ '_ \\  |  __  |/ _ \\| |/ _` |/ _ \\ '_ ` _ \\\n"+ 
" | |__| | |_) |  __/ | | | | |  | | (_) | | (_| |  __/ | | | | |\n"+
"  \\____/| .__/ \\___|_| |_| |_|  |_|\\___/|_|\\__,_|\\___|_| |_| |_|\n"+
"        | |\n" +
"        |_| \n\n\n\n\n";
	private static final String USER = "Choose a nickname";
	private PlayerUI ui;
	private PrintWriter out;
	private BufferedReader in;
	private Game game;
	/**
	 * @param in
	 * @param out
	 * @param game
	 * @param ui 
	 */
	public Login(BufferedReader in, PrintWriter out, Game game, TextSocketUI ui) {
		this.in = in;
		this.out = out;
		this.game=game;
		this.ui=ui;
	}


	/**
	 * @throws IOException 
	 * 
	 */
	public Player getPlayer() throws IOException {
		this.out.println(greeting());
		this.out.println(USER);
		String inputLine = this.in.readLine();
		return new Player(Thread.currentThread().hashCode(),inputLine,Game.DEFAULT_CHIPS,ui,game);
	}

	/**
	 * @param ui
	 */
	public void setUI(PlayerUI ui) {
		this.ui=ui;
	}
	private String greeting() {
		return BANNER+">>>>>> Welcome to OpenHoldem <<<<<<";
	}
}
