/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.network;

import java.util.Iterator;
import java.util.List;

import es.tems.openholdem.Game;
import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerAction;
import es.tems.openholdem.PlayerUI;
import es.tems.openholdem.event.FlopEvent;
import es.tems.openholdem.event.GameEvent;
import es.tems.openholdem.event.NewPlayerEvent;
import es.tems.openholdem.event.PlayerActionEvent;
import es.tems.openholdem.event.PotEvent;
import es.tems.openholdem.event.ShowEvent;
import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.Hand;

/**
 * 
 * A user interface that communicates through a socket and provides text-mode
 * interaction
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 * 
 */
public class TextSocketUI implements PlayerUI {

	private static final long TIMEOUT = 150000;
	private static final int IDLE = 0;
	private static final int WAITING = 1;
	private static final int CLOSE = 2;

	private ServerThread serverThread;
	TextScreen screen = new TextScreen();

	private String line;
	private int state = IDLE;

	/**
	 * @param serverThread
	 */
	public TextSocketUI(ServerThread serverThread) {
		this.serverThread = serverThread;
	}

	public void setServer(ServerThread serverThread) {
		this.serverThread = serverThread;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.tems.openholdem.PlayerUI#askForAction(es.tems.openholdem.Player,
	 * java.util.ArrayList)
	 */
	@Override
	public synchronized PlayerAction askForAction(Player player,
			List<Integer> allowedActions) {
		initScreen(player, player.getGame());
		printAllowedActions(allowedActions);
		
		printScreen();
		PlayerAction pa = null;
		try {
			while (true) {
				state = WAITING;
				this.wait(TIMEOUT);
				if (state == WAITING) {
					// timeout
					state = IDLE;
					return new PlayerAction(player, PlayerAction.FOLD, 0);
				} else if (state == CLOSE){
					return new PlayerAction(player, PlayerAction.JUSTLEFT, 0);
				}
				// else user response
				if (line.length() == 0)
					continue;
				try {

					pa = parse(player, line);
				} catch (NumberFormatException e) {
					screen.addNotifications("This action must be followed by a positive integer number.");
					printAllowedActions(allowedActions);
					printScreen();
					continue;
				}
				// comment for hackers delight
				if ((pa.getAction() == PlayerAction.RAISE)
						&& (pa.getAmount() <= 0)) {
					screen.addNotifications("This action must be followed by a positive integer number.");
					printAllowedActions(allowedActions);
					printScreen();
					continue;
				}
				if (allowedActions.contains(pa.getAction()))
					break;
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Wait for signalling from TimeOut or proccessInput.
		return pa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.tems.openholdem.PlayerUI#notify(es.tems.openholdem.Player,
	 * es.tems.openholdem.event.GameEvent)
	 */
	@Override
	public void notify(Player player, GameEvent gameEvent) {
		initScreen(player,gameEvent.getGame());
		if (gameEvent instanceof PlayerActionEvent) {
			PlayerAction pa = ((PlayerActionEvent) gameEvent).getPlayerAction();
			screen.addNotifications(pa.getPlayer().getNick() + " "
					+ pa.getActionString());

		} else if (gameEvent instanceof FlopEvent) {
			screen.addNotifications("New cards on the table!");

		} else if (gameEvent instanceof ShowEvent) {

			if (gameEvent.getGame().getShowdownPlayers().size() == 1)
				return;
			printShowdown(((ShowEvent) gameEvent).getOrderedHands());
			screen.setShowdown(true);

		} else if (gameEvent instanceof PotEvent) {
			PotEvent pe = (PotEvent) gameEvent;
			String winstring = pe.isTied() ? " is one of the winners of the "
					: " wins the ";
			screen.addNotifications(pe.getPlayer().getNick() + winstring + pe.getPotID()
					+ " pot and receives " + pe.getEarnings() + " chips.");
			screen.setShowdown(true);

		} else if (gameEvent instanceof NewPlayerEvent) {
			screen.addNotifications(((NewPlayerEvent) gameEvent).getPlayer()
					.getNick() + " has joined the table.");
		}
		printScreen();
	}

	private void initScreen(Player player, Game game) {
		screen.setPrompt("Waiting for your turn... ('!quit' to exit)");
		screen.setOwncards(player.getCards());
		screen.setPlayers(game.getPlayers());
		screen.setPot(game.getPot());
		screen.setSidepots(game.getSidePots());
		screen.setTablecards(game.getTableCards());
		screen.setShowdown(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.tems.openholdem.PlayerUI#setCards(es.tems.openholdem.Player,
	 * es.tems.openholdem.hand.Card, es.tems.openholdem.hand.Card)
	 */
	@Override
	public void setCards(Player player, Card card1, Card card2) {
		Card[] owncards = new Card[2];
		owncards[0] = card1;
		owncards[1] = card2;
		initScreen(player, player.getGame());
		printScreen();
	}

	// Protocol methods
	public String processInput(String inputLine) {
		if (inputLine.equals("!quit")) 
			return "Bye!";
		switch (state) {
		case IDLE:
			return idleInput(inputLine);
		case WAITING:
			return waitInput(inputLine);
		default:
			return "";
		}

	}

	public synchronized void endInterface() {
		state = CLOSE;
		this.notifyAll();
	}
	
	private PlayerAction parse(Player player, String line)
			throws NumberFormatException, IllegalArgumentException {
		short action = 0;
		int amount = 0;

		line = line.trim().toLowerCase();
		if (line.length() == 1) {

			if (line.charAt(0) == 'a')
				action = PlayerAction.ALL_IN_RAISE;
			else if (line.charAt(0) == 'l')
				action = PlayerAction.ALL_IN_CALL;
			else if (line.charAt(0) == 'c')
				action = PlayerAction.CALL;
			else if (line.charAt(0) == 'e')
				action = PlayerAction.CHECK;
			else if (line.charAt(0) == 'f')
				action = PlayerAction.FOLD;
		} else {
			if (line.charAt(0) == 'r') {
				line = line.substring(1).trim();
				amount = Integer.parseInt(line);
				action = PlayerAction.RAISE;
			}
		}
		return new PlayerAction(player, action, amount);
	}

	private synchronized String waitInput(String inputLine) {
		line = inputLine;
		this.state = IDLE;
		this.notifyAll();
		return "";
	}

	private String idleInput(String inputLine) {
		// TODO Some menu interaction... settings etc.
		screen.setPrompt("Still not your turn");
		printScreen();
		return "";
	}

	
	// Screen methods
	private String getTextForActions(List<Integer> allowedActions) {
		Iterator<Integer> it = allowedActions.iterator();
		String text = "";
		while (it.hasNext()) {
			switch (it.next()) {
			case (int) PlayerAction.ALL_IN_RAISE:
				text = text + " (a) for All In";
				break;
			case (int) PlayerAction.ALL_IN_CALL:
				text = text + " (l) for Call/All In";
				break;
			case (int) PlayerAction.CALL:
				text = text + " (c) for Call";
				break;
			case (int) PlayerAction.CHECK:
				text = text + " (e) for Check";
				break;
			case (int) PlayerAction.FOLD:
				text = text + " (f) for Fold";
				break;
			case (int) PlayerAction.RAISE:
				text = text + " (r) or (r <amount>) for Raise";
				break;
			}
		}

		return text;
	}



	private void printAllowedActions(List<Integer> allowedActions) {
		String allowedstring = getTextForActions(allowedActions);
		screen.setPrompt("Your turn:" + allowedstring
				+ " and hit ENTER");
	}

	private void printShowdown(List<List<Hand>> hands) {
		for (List<Hand> tiedhands : hands)
			for (Hand hand : tiedhands) {
				screen.addNotifications(hand.getPlayer().getNick() + " has "
						+ hand.getDescription() + " with : | "
						+ hand.getPlayer().getCards()[0].toString()
						+ " |    | "
						+ hand.getPlayer().getCards()[1].toString()
						+ " | in his hand");
			}

	}


	private void printScreen() {
		serverThread.processOutput("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		for(String outputLine: screen.getLines())
			serverThread.processOutput(outputLine);
	}

	

}
