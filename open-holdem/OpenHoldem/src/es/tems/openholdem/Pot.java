/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import java.util.List;

public class Pot {

	private int amount;
	private List<Player> players;
	private String potID;
	
	
	public Pot(int amount, List<Player> players, String potID) {
		super();
		this.amount = amount;
		this.players = players;
		this.potID=potID;
	}
	public String getPotID() {
		return potID;
	}
	public void setPotID(String potID) {
		this.potID = potID;
	}

	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	public void removePlayer(Player player) {
		this.players.remove(player);
	}
	
}
