/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import es.tems.openholdem.exception.InconsistentStateException;
import es.tems.openholdem.network.Server;
import es.tems.openholdem.test.LocalConsolePlayerUI;
import es.tems.util.CircularArrayList;


public class Main {
	public static void main(String args[]){

		Log log = Log.getInstance();
		log.info("Starting OpenHoldem server...");
		//Configure Game
		Game game = new Game();
		new Thread( new Server( game)).start();
		while(true){
			try {
				game.playHand();
			} catch (InconsistentStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Start server
		//testarrays();
		//test();
	}

	public static void test(){
		
		
		
		Game game = new Game();
		Player p1= new Player(1,"arc",500,LocalConsolePlayerUI.getInstance(),game);
		Player p2= new Player(2,"Linus",500,LocalConsolePlayerUI.getInstance(),game);
		Player p3= new Player(3,"Dennis",500,LocalConsolePlayerUI.getInstance(),game);
		Player p4= new Player(4,"Richard",500,LocalConsolePlayerUI.getInstance(),game);
		
		CircularArrayList<Player> players= new CircularArrayList<Player>(); 
		
		players.add(p1);
		players.add(p2);
		players.add(p3);
		players.add(p4);
		
		
		
		game.setPlayers(players);
		try {
			game.playHand();
		} catch (InconsistentStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

