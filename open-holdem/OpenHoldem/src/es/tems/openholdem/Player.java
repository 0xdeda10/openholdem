/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem;

import java.util.ArrayList;

import es.tems.openholdem.event.GameEvent;
import es.tems.openholdem.exception.InconsistentStateException;
import es.tems.openholdem.hand.Card;
import es.tems.util.CircularArrayList;

public class Player {

	int ID;
	Game game;
	String nick;
	Card cards[] = new Card[2];
	int total_chips;
	int tablechips;
	PlayerAction lastPlayerAction;
	PlayerUI ui;
	private boolean spectator;

	public Player(int iD, String nick, int total_chips, PlayerUI ui, Game game) {
		super();
		ID = iD;
		this.nick = nick;
		this.total_chips = total_chips;
		this.ui = ui;
		this.game = game;
	}

	// Getters and Setters
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void setNick(String nickname) {
		nick = nickname;
	}

	public String getNick() {
		return nick;
	}

	public Card[] getCards() {
		return cards;
	}

	public void setCards(Card card1, Card card2) {
		this.cards[0] = card1;
		this.cards[1] = card2;
		ui.setCards(this, card1, card2);
	}

	public int getTotal_chips() {
		return total_chips;
	}

	public void setTotal_chips(int total_chips) {
		this.total_chips = total_chips;
	}

	public PlayerAction getLastPlayerAction() {
		return lastPlayerAction;
	}

	public void setLastPlayerAction(PlayerAction lastPlayerAction) {
		this.lastPlayerAction = lastPlayerAction;
	}

	public int getTablechips() {
		return tablechips;
	}

	public void setTablechips(int tablechips) {
		this.tablechips = tablechips;
	}

	// Public Methods
	public PlayerAction askForAction(int currentstake, CircularArrayList<Player> players)
			throws InconsistentStateException {
		PlayerAction pa;
		if ( this.lastPlayerAction !=null ){
			if (this.lastPlayerAction.getAction() == PlayerAction.JUSTLEFT)
				return this.lastPlayerAction;
		}
		ArrayList<Integer> allowedActions = this.getAllowedActions(currentstake);
		
		int bet = 0;
		while (true) {
			pa = ui.askForAction(this, allowedActions);
			if (pa.getAction() == PlayerAction.ALL_IN_CALL) {
				this.betChips(total_chips);
				break;
			}
			if (pa.getAction() == PlayerAction.ALL_IN_RAISE) {
				pa.setAmount(total_chips);
				this.betChips(total_chips);
				break;
			}
			if ((pa.getAction() == PlayerAction.CALL)) {
				bet = currentstake - this.tablechips;
				if (bet == this.total_chips)
					pa.setAction(PlayerAction.ALL_IN_CALL);
			}
			if ((pa.getAction() == PlayerAction.RAISE)) {
				bet = currentstake - this.tablechips + pa.getAmount();
				if (bet == this.total_chips)
					pa.setAction(PlayerAction.ALL_IN_RAISE);
			}
			if (bet > total_chips)
				continue;
			this.betChips(bet);
			break;
		}

		this.lastPlayerAction = pa;
		return pa;
	}

	private ArrayList<Integer> getAllowedActions(int currentstake) throws InconsistentStateException {
		ArrayList<Integer> allowedActions = new ArrayList<Integer>();
		if (currentstake > tablechips) {
			if (total_chips > currentstake - tablechips) {
				allowedActions.add(new Integer(PlayerAction.ALL_IN_RAISE));
				allowedActions.add(new Integer(PlayerAction.RAISE));
				allowedActions.add(new Integer(PlayerAction.CALL));
			} else
				allowedActions.add(new Integer(PlayerAction.ALL_IN_CALL));

			allowedActions.add(new Integer(PlayerAction.FOLD));

		} else if (currentstake == tablechips) {
			allowedActions.add(new Integer(PlayerAction.ALL_IN_RAISE));
			allowedActions.add(new Integer(PlayerAction.RAISE));
			allowedActions.add(new Integer(PlayerAction.CHECK));
		} else
			throw new InconsistentStateException("Asking for action when table chips are higher than current stake");
		return allowedActions;
	}

	private void betChips(int amount) {
		total_chips = total_chips - amount;
		this.setTablechips(tablechips + amount);
	}

	public PlayerAction betSmallBlind(short amount) {
		this.betChips(amount);
		this.lastPlayerAction = new PlayerAction(this, PlayerAction.SMALL_BLIND, amount);
		return this.lastPlayerAction;
	}

	public PlayerAction betBigBlind(short amount) {
		this.betChips(amount);
		this.lastPlayerAction = new PlayerAction(this, PlayerAction.BIG_BLIND, amount);
		return this.lastPlayerAction;
	}

	// public void notify(PlayerAction pa, int stake) {
	// ui.notify(this,pa, stake);
	//
	// }

	public void notify(GameEvent gameEvent) {
		ui.notify(this, gameEvent);

	}

	public void reset() {
		lastPlayerAction = null;
		tablechips = 0;
	}

	public boolean isInactive() {
		// A player is inactive if he has folded or has no more chips to bet
		if (lastPlayerAction == null)
			return false;
		return isAllIn() || !hasActiveBet();
	}

	public void setSpectator(boolean b) {
		this.spectator = b;

	}

	public boolean isSpectator() {
		return this.spectator;
	}

	/**
	 * @return
	 */
	public boolean hasActiveBet() {
		if (lastPlayerAction == null)
			return false;
		return !((lastPlayerAction.getAction() == PlayerAction.FOLD)
				|| (lastPlayerAction.getAction() == PlayerAction.ABSENT) || (lastPlayerAction.getAction() == PlayerAction.JUSTLEFT));
	}

	/**
	 * @return
	 */
	public boolean isAllIn() {
		if (lastPlayerAction == null)
			return false;
		return ((lastPlayerAction.getAction() == PlayerAction.ALL_IN_CALL) || (lastPlayerAction.getAction() == PlayerAction.ALL_IN_RAISE));
	}

}
