/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.util;

import java.util.Iterator;

public class CircularArrayListIterator<E> implements Iterator<E> {
	private CircularArrayList<E> cal;
	private int index=0;
	
	public CircularArrayListIterator(CircularArrayList<E> circularArrayList) {
		this.cal=new CircularArrayList<E>(circularArrayList);
	}

	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public E next() {
		return cal.get(index++);
	}

	@Override
	public void remove() {
		 this.cal.remove(index);
	}

}
