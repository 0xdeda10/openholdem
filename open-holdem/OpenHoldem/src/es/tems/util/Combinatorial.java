/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Combinatorial<E> {

	Set<E> set;



	public Combinatorial(Set<E> set) {
		super();
		this.set = set;
	}
	// Getters and Setters
	public Set<E> getSet() {
		return set;
	}

	public void setSet(Set<E> set) {
		this.set = set;
	}
	
	public Set<Set<E>> getCombinations(int n){
		return	combinations(set,n);
	}
	private Set<Set<E>> combinations(Set<E> set, int n) {
		Set<Set<E>> combinations = null;
		// C_0 (S) = { {} }
		if (n==0)  {
			combinations = new HashSet<Set<E>>();
			combinations.add((Set<E>) new HashSet<Set<E>>());
			return combinations;
		}

		//  C_n(S) = {{S}} n>=size(S)
		if (n>=set.size()) {
			combinations = new HashSet<Set<E>>();
			combinations.add(set);
			return combinations;
		}
		// C_n(S) = e*C_(n-1)(S-{e})  U C_n(S-{e})
		List<E> listWithoutElement = new ArrayList<E>(set);
		E element = listWithoutElement.remove(0);
		
		Set<E> setWithoutElement = new HashSet<E>();
		setWithoutElement.addAll(listWithoutElement);
		
		Set<Set<E>> combinationsWithElement = combinations(setWithoutElement,n-1);
		for(Set<E> combs: combinationsWithElement)
			combs.add(element);
		
		Set<Set<E>> combinationsWithoutElement = combinations(setWithoutElement,n);
		
		combinations = new HashSet<Set<E>>();
		combinations.addAll(combinationsWithElement);
		combinations.addAll(combinationsWithoutElement);
		
		return combinations;
	}
	
	
	
}
