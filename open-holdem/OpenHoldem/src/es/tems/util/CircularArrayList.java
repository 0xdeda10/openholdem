/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class CircularArrayList<E> extends ArrayList<E>{
	private static final long serialVersionUID = 1L;
    
	public CircularArrayList() {
		super();
	}

	public CircularArrayList(Collection<E> c) {
		super(c);
	}

	public E get(int index)
    {
        return super.get(getBaseIndex(index));
    }
    public Iterator<E> iterator(){
		return new CircularArrayListIterator<E>(this);
    	
    }
	public Iterator<E> getArrayListIterator() {
		return super.iterator();
	}
	public int getBaseIndex(int index){
		int newindex = index % size();
    	if (newindex<0) newindex=newindex + size(); 
    	return newindex;
	}
	
	
}
