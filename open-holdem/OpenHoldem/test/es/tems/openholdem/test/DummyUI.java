/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.test;

import java.util.ArrayList;
import java.util.List;

import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerAction;
import es.tems.openholdem.PlayerUI;
import es.tems.openholdem.event.GameEvent;
import es.tems.openholdem.hand.Card;

public class DummyUI implements PlayerUI {


	@Override
	public void notify(Player player, GameEvent gameEvent) {

	}

	@Override
	public void setCards(Player player, Card card1, Card card2) {
	}

	@Override
	public PlayerAction askForAction(Player player, List<Integer> allowedActions) {
		// TODO Auto-generated method stub
		return null;
	}

}
