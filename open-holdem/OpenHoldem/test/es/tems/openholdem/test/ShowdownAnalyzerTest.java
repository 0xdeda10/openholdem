/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

import es.tems.openholdem.Player;
import es.tems.openholdem.ShowdownAnalyzer;
import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.FrenchCard;
import es.tems.openholdem.hand.Hand;

/**
 * 
 * 
 * 
 * @author Álvaro Ramos (alvaro@pimedios.com)
 * 
 */
public class ShowdownAnalyzerTest {

	public static final Card[] TABLECARDS1 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 1),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) 11, (short) 3),
			new FrenchCard((short) 10, (short) 1),
			new FrenchCard((short) FrenchCard.ACE, (short) 0) 
			};

	
	public static final Card[] FIVECARDS1 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 1),
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) FrenchCard.ACE, (short) 3),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) FrenchCard.ACE, (short) 0) 
	};
	public static final Card[] FIVECARDS2 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 1),
			new FrenchCard((short) 10, (short) 2), 
			new FrenchCard((short) 10, (short) 3),
			new FrenchCard((short) 10, (short) 1),
			new FrenchCard((short) FrenchCard.ACE, (short) 0) 
	};
	
//	public static final Card[] FIVECARDS3 = {
//			new FrenchCard((short) 12, (short) 0),
//			new FrenchCard((short) 11, (short) 3),
//			new FrenchCard((short) 9, (short) 2), 
//			new FrenchCard((short) 13, (short) 3),
//			new FrenchCard((short) 10, (short) 1)
//	};
//	public static final Card[] FIVECARDS4 = {
//			new FrenchCard((short) 12, (short) 0),
//			new FrenchCard((short) 11, (short) 3),
//			new FrenchCard((short) 9, (short) 0), 
//			new FrenchCard((short) 13, (short) 1),
//			new FrenchCard((short) 10, (short) 1)
//	};
//	
	public static List<Card> tablecards1;
	public static List<Player> showdownPlayers1;

	@BeforeClass
	public static void setUp() {
		tablecards1 = new ArrayList<Card>(Arrays.asList(TABLECARDS1));
		showdownPlayers1 = new ArrayList<Player>();

		// 4 of a Kind
		Player player = new Player(0, "4Player", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) FrenchCard.ACE, (short) 2),
				new FrenchCard((short) FrenchCard.ACE, (short) 3));
		showdownPlayers1.add(player);

		// FullHouse 10,10,10,A,A
		player = new Player(0, "FullPlayer", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 10, (short) 2), new FrenchCard(
				(short) 10, (short) 3));
		showdownPlayers1.add(player);

		// Straight 10,J,Q,K,A(TIED)
		player = new Player(0, "StraightTied1", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 9, (short) 2), new FrenchCard(
				(short) 13, (short) 3));
		showdownPlayers1.add(player);

		// Straight 10,J,Q,K,A (TIED)
		player = new Player(0, "StraightTied2", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 9, (short) 0), new FrenchCard(
				(short) 13, (short) 1));
		showdownPlayers1.add(player);

		// Straight 8,9,10,J,Q
		player = new Player(0, "Straightloser", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 9, (short) 2), new FrenchCard(
				(short) 8, (short) 3));
		showdownPlayers1.add(player);

		// Two Pair 2,2,A,A,Q
		player = new Player(0, "TwoPair", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 2, (short) 2), new FrenchCard(
				(short) 2, (short) 3));
		showdownPlayers1.add(player);

		// Pair A,A,Q,J,10
		player = new Player(0, "PairTied1", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 3, (short) 2), new FrenchCard(
				(short) 4, (short) 3));
		showdownPlayers1.add(player);

		// Pair A,A,Q,J,10
		player = new Player(0, "PairTied2", 0, new DummyUI(), null);
		player.setCards(new FrenchCard((short) 3, (short) 1), new FrenchCard(
				(short) 5, (short) 3));
		showdownPlayers1.add(player);

	}

	@Test
	public void testHands() {
		ShowdownAnalyzer s = new ShowdownAnalyzer(tablecards1, showdownPlayers1);
		List<List<Hand>> hands = s.getOrderedHands();
		boolean b = hands.get(0).get(0).getFivecards().containsAll(Arrays.asList(FIVECARDS1)) &&
				hands.get(1).get(0).getFivecards().containsAll(Arrays.asList(FIVECARDS2));//&&
				//winners.get(2).get(0).getFivecards().containsAll(Arrays.asList(FIVECARDS3));//||winners.get(2).get(1).getFivecards().containsAll(Arrays.asList(FIVECARDS3)));
		//(winners.get(2).get(0).getFivecards().containsAll(Arrays.asList(FIVECARDS4))||winners.get(2).get(1).getFivecards().containsAll(Arrays.asList(FIVECARDS4))) ;
		
		System.out.println(hands);
		assertTrue(b);
	}
	@Test
	public void testWinners() {
		ShowdownAnalyzer s = new ShowdownAnalyzer(tablecards1, showdownPlayers1);
		List<List<Player>> winners = s.getWinners();
		boolean b = winners.get(0).get(0).getNick().equals("4Player") &&
				winners.get(1).get(0).getNick().equals("FullPlayer") &&
				winners.get(2).get(0).getNick().contains("StraightTied") &&
				winners.get(2).get(1).getNick().contains("StraightTied")&&
				winners.get(3).get(0).getNick().equals("Straightloser")&&
				winners.get(4).get(0).getNick().equals("TwoPair")&&
				winners.get(5).get(0).getNick().contains("PairTied")&&
				winners.get(5).get(1).getNick().contains("PairTied");
				
		System.out.println(winners);
		assertTrue(b);
	}
}
