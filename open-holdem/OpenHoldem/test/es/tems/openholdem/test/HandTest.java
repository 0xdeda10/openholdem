/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import es.tems.openholdem.Player;
import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.Flush;
import es.tems.openholdem.hand.FourOfAKind;
import es.tems.openholdem.hand.FrenchCard;
import es.tems.openholdem.hand.OnePair;
import es.tems.openholdem.hand.Straight;
import es.tems.openholdem.hand.StraightFlush;
import es.tems.openholdem.hand.FullHouse;
import es.tems.openholdem.hand.ThreeOfAKind;
import es.tems.openholdem.hand.TwoPair;
import es.tems.util.Combinatorial;

public class HandTest {

	public static final String[] STRINGS1 = { "a", "b", "c", "d", "e" };

	public static final Card[] TABLECARDS1 = {
			new FrenchCard((short) 2, (short) 0),
			new FrenchCard((short) 13, (short) 0),
			new FrenchCard((short) 5, (short) 3),
			new FrenchCard((short) 8, (short) 1),
			new FrenchCard((short) 14, (short) 0), };

	public static final Card[] PLAYERCARDS1 = {
			new FrenchCard((short) 10, (short) 0),
			new FrenchCard((short) 8, (short) 0) };
	
	public static final Card[] FIVECARDS1 = {
		new FrenchCard((short) 2, (short) 0),
		new FrenchCard((short) 13, (short) 0),
		new FrenchCard((short) 10, (short) 0),
		new FrenchCard((short) 8, (short) 0),
		new FrenchCard((short) 14, (short) 0), };

	private static final Card[] TABLECARDS2 = {
			new FrenchCard((short) 2, (short) 0),
			new FrenchCard((short) 13, (short) 1),
			new FrenchCard((short) 5, (short) 3),
			new FrenchCard((short) 8, (short) 1),
			new FrenchCard((short) 14, (short) 0), };

	private static final Card[] PLAYERCARDS2 = {
			new FrenchCard((short) 10, (short) 1),
			new FrenchCard((short) 8, (short) 0) };

	private static final Card[] TABLECARDS3 = {
			new FrenchCard((short) 2, (short) 0),
			new FrenchCard((short) 13, (short) 1),
			new FrenchCard((short) 2, (short) 1),
			new FrenchCard((short) 8, (short) 1),
			new FrenchCard((short) 14, (short) 0), };

	private static final Card[] PLAYERCARDS3 = {
			new FrenchCard((short) 2, (short) 2),
			new FrenchCard((short) 2, (short) 3) };

	private static final Card[] FIVECARDS3 = {
			new FrenchCard((short) 14, (short) 0),
			new FrenchCard((short) 2, (short) 0),
			new FrenchCard((short) 2, (short) 1),
			new FrenchCard((short) 2, (short) 2),
			new FrenchCard((short) 2, (short) 3) };

	private static final Card[] TABLECARDS4 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 0),
			new FrenchCard((short) 10, (short) 0),
			new FrenchCard((short) 13, (short) 0),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 6, (short) 1), };

	private static final Card[] PLAYERCARDS4 = {
			new FrenchCard((short) 11, (short) 0),
			new FrenchCard((short) 12, (short) 0) };
	
	private static final Card[] FIVECARDS4 = {
		new FrenchCard((short) FrenchCard.ACE, (short) 0),
		new FrenchCard((short) 10, (short) 0),
		new FrenchCard((short) 11, (short) 0),
		new FrenchCard((short) 12, (short) 0),
		new FrenchCard((short) 13, (short) 0) };
	

	private static final Card[] TABLECARDS5 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) 10, (short) 3),
			new FrenchCard((short) 13, (short) 0),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 6, (short) 1), };

	private static final Card[] PLAYERCARDS5 = {
			new FrenchCard((short) 11, (short) 1),
			new FrenchCard((short) 12, (short) 0) };
	
	private static final Card[] FIVECARDS5 = {
		new FrenchCard((short) FrenchCard.ACE, (short) 2),
		new FrenchCard((short) 10, (short) 3),
		new FrenchCard((short) 13, (short) 0),
		new FrenchCard((short) 11, (short) 1),
		new FrenchCard((short) 12, (short) 0) };

	private static final Card[] TABLECARDS6 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 13, (short) 0),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 9, (short) 1), };

	private static final Card[] PLAYERCARDS6 = {
			new FrenchCard((short) 12, (short) 1),
			new FrenchCard((short) 12, (short) 0) };

	private static final Card[] FIVECARDS6 = {
			new FrenchCard((short) 12, (short) 1),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 9, (short) 1), };

	private static final Card[] TABLECARDS7 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 10, (short) 2),
			new FrenchCard((short) 8, (short) 1), };

	private static final Card[] PLAYERCARDS7 = {
			new FrenchCard((short) 12, (short) 1),
			new FrenchCard((short) 12, (short) 0) };

	private static final Card[] FIVECARDS7 = {
			new FrenchCard((short) 12, (short) 1),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 10, (short) 2),
			new FrenchCard((short) FrenchCard.ACE, (short) 2) };

	private static final Card[] TABLECARDS8 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 9, (short) 0),
			new FrenchCard((short) 10, (short) 2),
			new FrenchCard((short) 8, (short) 1), };

	private static final Card[] PLAYERCARDS8 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 0),
			new FrenchCard((short) 12, (short) 0) };

	private static final Card[] FIVECARDS8 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) FrenchCard.ACE, (short) 0),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) 12, (short) 3),
			new FrenchCard((short) 10, (short) 2) };
	
	private static final Card[] TABLECARDS9 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) 6, (short) 3),
			new FrenchCard((short) 11, (short) 3),
			new FrenchCard((short) 10, (short) 2),
			new FrenchCard((short) 8, (short) 1), };

	private static final Card[] PLAYERCARDS9 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 0),
			new FrenchCard((short) 12, (short) 0) };

	private static final Card[] FIVECARDS9 = {
			new FrenchCard((short) FrenchCard.ACE, (short) 2),
			new FrenchCard((short) FrenchCard.ACE, (short) 0),
			new FrenchCard((short) 12, (short) 0),
			new FrenchCard((short) 11, (short) 3),
			new FrenchCard((short) 10, (short) 2) };
	
	private static final Class<?>[] parameterTypes = { Player.class, Set.class };

	@Test
	public void testStraightFlush() {
		Card[] c = TABLECARDS4;
		Card[] player_cards = PLAYERCARDS4;
		StraightFlush instance = ((StraightFlush) getInstance(c, player_cards,
				StraightFlush.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS4));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void test4OfAKind() {
		Card[] c = TABLECARDS3;
		Card[] player_cards = PLAYERCARDS3;
		FourOfAKind instance = ((FourOfAKind) getInstance(c, player_cards,
				FourOfAKind.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS3));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testFullHouse() {
		Card[] c = TABLECARDS6;
		Card[] player_cards = PLAYERCARDS6;
		FullHouse instance = ((FullHouse) getInstance(c, player_cards,
				FullHouse.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS6));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testFlush() {
		Card[] c = TABLECARDS1;
		Card[] player_cards = PLAYERCARDS1;
		Flush instance = (Flush) getInstance(c, player_cards, Flush.class);
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS1));
		System.out.println(b);
		assertTrue(b);

	}

	@Test
	public void testNoFlush() {
		Card[] c = TABLECARDS2;
		Card[] player_cards = PLAYERCARDS2;
		assertFalse(((Flush) getInstance(c, player_cards, Flush.class))
				.hasHand());
	}

	@Test
	public void testStraight() {
		Card[] c = TABLECARDS5;
		Card[] player_cards = PLAYERCARDS5;
		Straight instance = ((Straight) getInstance(c, player_cards,
				Straight.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS5));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testThreeOfAKind() {
		Card[] c = TABLECARDS7;
		Card[] player_cards = PLAYERCARDS7;
		ThreeOfAKind instance = ((ThreeOfAKind) getInstance(c, player_cards,
				ThreeOfAKind.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS7));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testTwoPair() {
		Card[] c = TABLECARDS8;
		Card[] player_cards = PLAYERCARDS8;
		TwoPair instance = ((TwoPair) getInstance(c, player_cards,
				TwoPair.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS8));

		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testOnePair() {
		Card[] c = TABLECARDS9;
		Card[] player_cards = PLAYERCARDS9;
		OnePair instance = ((OnePair) getInstance(c, player_cards,
				OnePair.class));
		boolean b = instance.hasHand();
		Set<Card> fivecards = instance.getFivecards();
		b = b && fivecards.containsAll(Arrays.asList(FIVECARDS9));
		System.out.println(b);
		assertTrue(b);
	}

	@Test
	public void testCombinational() {
		Set<String> set = new HashSet<String>();
		set.addAll(Arrays.asList(STRINGS1));
		Combinatorial<String> c = new Combinatorial<String>(set);
		Set<Set<String>> combs = c.getCombinations(3);
		System.out.println(combs);
		boolean b = true;
		for (Set<String> oneComb : combs)
			if (oneComb.size() != 3)
				b = false;
		assertTrue(b);
		assertEquals("Number of combinations should be 10", 10, combs.size());
	}

	private Object getInstance(Card[] c, Card[] player_cards, Class<?> handclass) {

		Set<Card> cards = new HashSet<Card>();
		cards.addAll(Arrays.asList(c));
		Player player = new Player(0, "", 0, new DummyUI(), null);
		player.setCards(player_cards[0], player_cards[1]);
		Constructor<?> ctor = null;
		try {
			ctor = handclass.getConstructor(parameterTypes);
			return ctor.newInstance(player, cards);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
