/*
 *  OpenHoldem, a simple Texas Holdem Multiplayer Game Engine
    Copyright (C) 2015  Álvaro Ramos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package es.tems.openholdem.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import es.tems.openholdem.Player;
import es.tems.openholdem.PlayerAction;
import es.tems.openholdem.PlayerUI;
import es.tems.openholdem.event.FlopEvent;
import es.tems.openholdem.event.GameEvent;
import es.tems.openholdem.event.PlayerActionEvent;
import es.tems.openholdem.event.PotEvent;
import es.tems.openholdem.event.ShowEvent;
import es.tems.openholdem.hand.Card;
import es.tems.openholdem.hand.Hand;

public class LocalConsolePlayerUI implements PlayerUI {

	private static LocalConsolePlayerUI instance = null;

	Scanner scanner = new Scanner(System.in);

	private LocalConsolePlayerUI() {

	}

	public static LocalConsolePlayerUI getInstance() {
		if (instance == null)
			instance = new LocalConsolePlayerUI();
		return instance;
	}

	@Override
	public PlayerAction askForAction(Player player,  List<Integer> allowedActions) {
		//
		System.out.println("--------------------------- >> to "+player.getNick()+" << ---------------------------------------");
		System.out.println("AskForAction-->player" + player.getID());
		printGameState(player);

		System.out.println("Action required:");
		String allowedstring = getTextForActions(allowedActions);
		System.out.println("you may type" + allowedstring + " and hit ENTER");

		scanner.useDelimiter(System.getProperty("line.separator"));
		String line;
		PlayerAction pa = null;
		boolean repeatprompt = true;
		while (repeatprompt) {
			line = scanner.next();
			try {
				pa = parse(player, line);
			} catch (NumberFormatException e) {
				System.out.println("This action must be followed by a positive integer number");
				System.out.println("you may type" + allowedstring + " and hit ENTER");
				continue;
			}
			// comment for hackers delight
			if ((pa.getAction()==PlayerAction.RAISE)&&(pa.getAmount() <= 0)) {
				System.out.println("This action must be followed by a positive integer number");
				System.out.println("you may type" + allowedstring + " and hit ENTER");
				continue;
			}
			if (allowedActions.contains(pa.getAction()) )
				repeatprompt = false;
		}

		System.out.println("------------------------------------------------------------------");

		return pa;
	}

	@Override
	public void notify(Player player, GameEvent gameEvent) {
		System.out.println("--------------------------- >> to "+player.getNick()+" << ---------------------------------------");
		
		if (gameEvent instanceof PlayerActionEvent) {
			PlayerAction pa = ((PlayerActionEvent)gameEvent).getPlayerAction();
			int stake = gameEvent.getGame().getCurrentStake();
						System.out.println(">>>>> NOTIFICATION >>>> New Action: Player " + pa.getPlayer().getNick() + " "
					+ pa.getActionString() + " current bet:" + stake);
			printGameState(player);
		} else if (gameEvent instanceof FlopEvent){
			System.out.println(">>>>> NOTIFICATION >>>> FLOP! <<<<");
			Iterator<Card> it = gameEvent.getGame().getTableCards().iterator();
			String text="CARDS on the TABLE : | ";
			while(it.hasNext()){
				text=text + it.next().toString()+ " | ";
			}
			System.out.println(text);
		} else if (gameEvent instanceof ShowEvent){
			if (gameEvent.getGame().getShowdownPlayers().size()==1) return;
			System.out.println(">>>>> NOTIFICATION >>>> SHOWDOWN! <<<<");
			Iterator<Card> it = gameEvent.getGame().getTableCards().iterator();
			String text="CARDS on the TABLE : | ";
			while(it.hasNext()){
				text=text + it.next().toString()+ " | ";
			}
			System.out.println(text);
			List<List<Hand>> hands = ((ShowEvent) gameEvent).getOrderedHands();
			
			for(List<Hand> tiedhands: hands)
				for(Hand hand: tiedhands){
					System.out.println(">>>>> "+hand.getPlayer().getNick()+" has " + hand.getDescription() + " with : | " + hand.getPlayer().getCards()[0].toString() + " |    | " + hand.getPlayer().getCards()[1].toString() + " | in his hand");
					
				}
			
			
		} else if ( gameEvent instanceof PotEvent){
			PotEvent pe= (PotEvent) gameEvent;
			String winstring = pe.isTied()?" is one of the winners of the ":" wins the ";
			System.out.println(">>>>> NOTIFICATION >>>> Pot distribution <<<<");
			System.out.println(">>>>> "+ pe.getPlayer().getNick()+ winstring + pe.getPotID() + " pot and receives "+ pe.getEarnings()+" chips");
		}
		System.out.println("------------------------------------------------------------------");


	}

	@Override
	public void setCards(Player player, Card card1, Card card2) {
		System.out.println("--------------------------- >> to "+player.getNick()+" << ---------------------------------------");
		System.out.println("New cards for Player " + player.getNick());
		printGameState(player);
		System.out.println("------------------------------------------------------------------");

	}

	private PlayerAction parse(Player player, String line) throws NumberFormatException {
		short action = 0;
		int amount = 0;

		line = line.trim().toLowerCase();
		if (line.length() == 1) {

			if (line.charAt(0) == 'a')
				action = PlayerAction.ALL_IN_RAISE;
			else if (line.charAt(0) == 'c')
				action = PlayerAction.CALL;
			else if (line.charAt(0) == 'e')
				action = PlayerAction.CHECK;
			else if (line.charAt(0) == 'f')
				action = PlayerAction.FOLD;
		} else {
			if (line.charAt(0) == 'r') {
				line = line.substring(1).trim();
				amount = Integer.parseInt(line);
				action = PlayerAction.RAISE;
			}
		}

		return new PlayerAction(player, action, amount);
	}

	private String getTextForActions(List<Integer> allowedActions) {

		Iterator<Integer> it = allowedActions.iterator();
		String text = "";
		while (it.hasNext()) {
			switch (it.next()) {
			case (int) PlayerAction.ALL_IN_RAISE:
				text = text + " (a) for All In";
				break;

			case (int) PlayerAction.CALL:
				text = text + " (c) for Call";
				break;
			case (int) PlayerAction.CHECK:
				text = text + " (e) for Check";
				break;
			case (int) PlayerAction.FOLD:
				text = text + " (f) for Fold";
				break;
			case (int) PlayerAction.RAISE:
				text = text + " (r) or (r <amount>) for Raise";
				break;
			}
		}

		return text;
	}

	// Printing methods
	private void printGameState(Player myself) {
		Iterator<Player> it = myself.getGame().getPlayers().getArrayListIterator();
		Player player;
		while (it.hasNext()) {
			player = it.next();
			if (player.getID() != myself.getID())
				printPublicInfo(player);
		}
		printPrivateInfo(myself);
	}

	private void printPublicInfo(Player player) {
		System.out.println(">>>>>>");
		System.out.println("Nick: " + player.getNick());
		System.out.println("Total chips: " + player.getTotal_chips());
		System.out.println("Bet: " + player.getTablechips());
		PlayerAction pa = player.getLastPlayerAction();

		int amount;
		if (pa != null) {
			String actiontext = pa.getActionString();
			amount = pa.getAmount();
			if (amount != 0)
				actiontext = actiontext + " " + amount;
			System.out.println("LastAction: " + actiontext);
		}

	}

	private void printPrivateInfo(Player player) {
		System.out.println(">>>>>> You <<<<<<");
		System.out.println("Nick: " + player.getNick());
		System.out.println("Total chips: " + player.getTotal_chips());
		System.out.println("Bet: " + player.getTablechips());
		System.out.println(
				"Cards: | " + player.getCards()[0].toString() + " |    | " + player.getCards()[1].toString() + " | ");

	}

	public void finalize() throws Throwable {
		scanner.close();
		super.finalize();
	}
}
